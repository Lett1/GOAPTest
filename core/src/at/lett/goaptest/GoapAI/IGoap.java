package at.lett.goaptest.GoapAI;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Queue;

/**
 * Created by kilianstrommer on 28.05.16.
 */
public interface IGoap {

    HashSet<AbstractMap.SimpleEntry<String, Object>> getWorldState();

    HashSet<AbstractMap.SimpleEntry<String, Object>> createGoalState();

    void planFailed(HashSet<AbstractMap.SimpleEntry<String, Object>> failedGoal);

    void planFound(HashSet<AbstractMap.SimpleEntry<String, Object>> goal, Queue<GoapAction> actions);

    void actionsFinished();

    void planAborted(GoapAction aborter);

    boolean moveAgent(GoapAction nextAction);


}
