package at.lett.goaptest.GoapAI;

import at.lett.goaptest.Entities.Entity;

import java.util.Stack;

/**
 * Created by kilianstrommer on 28.05.16.
 */

public interface GoapFSMState
{
    void Update(GoapFSM fsm, Entity entity);
}

