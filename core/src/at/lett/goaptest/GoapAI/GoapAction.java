package at.lett.goaptest.GoapAI;

import at.lett.goaptest.Entities.Entity;

import java.util.*;

/**
 * Created by kilianstrommer on 27.05.16.
 */
public abstract class GoapAction {

    private HashSet<AbstractMap.SimpleEntry<String, Object>> preconditions;
    private HashSet<AbstractMap.SimpleEntry<String, Object>> effects;

    private boolean inRange = false;

    public float cost = 1f;

    public Entity target;

    public GoapAction()
    {
        this.preconditions = new HashSet<>();
        this.effects = new HashSet<>();
    }

    public void doReset()
    {
        inRange = false;
        target = null;
        reset();
    }

    /**
     * Reset any variables that need to be reset before planning happens again.
     */
    public abstract void reset();

    /**
     * Is the action done?
     */
    public abstract boolean isDone();

    /**
     * Procedurally check if this action can run. Not all actions
     * will need this, but some might.
     */
    public abstract boolean checkProceduralPrecondition(Entity agent);

    /**
     * Run the action.
     * Returns True if the action performed successfully or false
     * if something happened and it can no longer perform. In this case
     * the action queue should clear out and the goal cannot be reached.
     */
    public abstract boolean perform(Entity agent);

    /**
     * Does this action need to be within range of a target game object?
     * If not then the moveTo state will not need to run for this action.
     */
    public abstract boolean requiresInRange ();


    /**
     * Are we in range of the target?
     * The MoveTo state will set this and it gets reset each time this action is performed.
     */
    public boolean isInRange () {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }


    public void addPrecondition(String key, Object value) {
        preconditions.add(new AbstractMap.SimpleEntry<String, Object>(key, value));
    }


    public void removePrecondition(String key) {
        for (Iterator<AbstractMap.SimpleEntry<String, Object>> i = preconditions.iterator(); i.hasNext();) {
            AbstractMap.SimpleEntry<String, Object> entry = i.next();
            if (entry.getKey() == key) {
                i.remove();
            }
        }
    }


    public void addEffect(String key, Object value) {
        effects.add(new AbstractMap.SimpleEntry<String, Object>(key, value));
    }


    public void removeEffect(String key) {
        for (Iterator<AbstractMap.SimpleEntry<String, Object>> i = effects.iterator(); i.hasNext();) {
            AbstractMap.SimpleEntry<String, Object> entry = i.next();
            if (entry.getKey() == key) {
                i.remove();
            }
        }
    }


    public HashSet<AbstractMap.SimpleEntry<String, Object>> Preconditions() {
            return preconditions;
    }

    public HashSet<AbstractMap.SimpleEntry<String, Object>> Effects() {
            return effects;
    }



}
