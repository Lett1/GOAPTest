package at.lett.goaptest.GoapAI;

import at.lett.goaptest.Entities.Entity;

import java.util.*;

/**
 * Created by kilianstrommer on 27.05.16.
 */
public class GoapPlanner {

    /**
     * Plan what sequence of actions can fulfill the goal.
     * Returns null if a plan could not be found, or a list of the actions
     * that must be performed, in order, to fulfill the goal.
     */
    public Queue<GoapAction> plan(Entity agent,
                                  HashSet<GoapAction> availableActions,
                                  HashSet<AbstractMap.SimpleEntry<String, Object>> worldState,
                                  HashSet<AbstractMap.SimpleEntry<String, Object>> goal)
    {
        // reset the actions so we can start fresh with them
        for(GoapAction a : availableActions) {
            a.doReset ();
        }

        // check what actions can run using their checkProceduralPrecondition
        HashSet<GoapAction> usableActions = new HashSet<> ();
        for (GoapAction a : availableActions) {
            if ( a.checkProceduralPrecondition(agent) )
                usableActions.add(a);
        }

        // we now have all actions that can run, stored in usableActions

        // build up the tree and record the leaf nodes that provide a solution to the goal.
        List<Node> leaves = new ArrayList<>();

        // build graph
        Node start = new Node (null, 0, worldState, null);
        boolean success = buildGraph(start, leaves, usableActions, goal);

        if (!success) {
            // oh no, we didn't get a plan
            System.out.println("NO PLAN");
            return null;
        }

        // get the cheapest leaf
        Node cheapest = null;
        for (Node leaf : leaves) {
            if (cheapest == null)
                cheapest = leaf;
            else {
                if (leaf.runningCost < cheapest.runningCost)
                    cheapest = leaf;
            }
        }

        // get its node and work back through the parents
        List<GoapAction> result = new ArrayList<>();
        Node n = cheapest;
        while (n != null) {
            if (n.action != null) {
                result.add(0, n.action); // insert the action in the front
            }
            n = n.parent;
        }
        // we now have this action list in correct order

        Queue<GoapAction> queue = new LinkedList<>();
        for (GoapAction a : result) {
            queue.add(a);
        }

        // hooray we have a plan!
        return queue;
    }

    /**
     * Returns true if at least one solution was found.
     * The possible paths are stored in the leaves list. Each leaf has a
     * 'runningCost' value where the lowest cost will be the best action
     * sequence.
     */
    private boolean buildGraph (Node parent, List<Node> leaves, HashSet<GoapAction> usableActions,
                                HashSet<AbstractMap.SimpleEntry<String, Object>> goal)
    {
        boolean foundOne = false;

        // go through each action available at this node and see if we can use it here
        for (GoapAction action : usableActions) {

        // if the parent state has the conditions for this action's preconditions, we can use it here
        if ( inState(action.Preconditions(), parent.state) ) {

            // apply the action's effects to the parent state
            HashSet<AbstractMap.SimpleEntry<String,Object>> currentState = populateState (parent.state, action.Effects());
            //Debug.Log(GoapAgent.prettyPrint(currentState));
            Node node = new Node(parent, parent.runningCost+action.cost, currentState, action);

            if (inState(goal, currentState)) {
                // we found a solution!
                leaves.add(node);
                foundOne = true;
            } else {
                // not at a solution yet, so test all the remaining actions and branch out the tree
                HashSet<GoapAction> subset = actionSubset(usableActions, action);
                boolean found = buildGraph(node, leaves, subset, goal);
                if (found)
                    foundOne = true;
            }
        }
    }

        return foundOne;
    }

    /**
     * Create a subset of the actions excluding the removeMe one. Creates a new set.
     */
    private HashSet<GoapAction> actionSubset(HashSet<GoapAction> actions, GoapAction removeMe) {
        HashSet<GoapAction> subset = new HashSet<GoapAction> ();
        for (GoapAction a : actions) {
            if (!a.equals(removeMe))
                subset.add(a);
        }
        return subset;
    }

    /**
     * Check that all items in 'test' are in 'state'. If just one does not match or is not there
     * then this returns false.
     */
    private boolean inState(HashSet<AbstractMap.SimpleEntry<String, Object>> test,
                            HashSet<AbstractMap.SimpleEntry<String, Object>> state) {
        boolean allMatch = true;
        for(AbstractMap.SimpleEntry<String, Object> t : test) {
            boolean match = false;
            for (AbstractMap.SimpleEntry<String, Object> s : state) {
                if (s.equals(t)) {
                    match = true;
                    break;
                }
            }
            if (!match)
                allMatch = false;
        }
        return allMatch;
    }

    /**
     * Apply the stateChange to the currentState
     */
    private HashSet<AbstractMap.SimpleEntry<String, Object>> populateState(
            HashSet<AbstractMap.SimpleEntry<String, Object>> currentState,
            HashSet<AbstractMap.SimpleEntry<String, Object>> stateChange) {

        HashSet<AbstractMap.SimpleEntry<String, Object>> state = new HashSet<>();
        // copy the KVPs over as new objects
        for (AbstractMap.SimpleEntry<String, Object> s : currentState) {
            state.add(new AbstractMap.SimpleEntry<String, Object>(s.getKey(),s.getValue()));
        }

        for (AbstractMap.SimpleEntry<String, Object> change : stateChange) {
            // if the key exists in the current state, update the Value
            boolean exists = false;

            for (AbstractMap.SimpleEntry<String, Object> s : state) {
                if (s.equals(change)) {
                    exists = true;
                    break;
                }
            }

            if (exists) {
                for (Iterator<AbstractMap.SimpleEntry<String, Object>> i = state.iterator(); i.hasNext();) {
                    AbstractMap.SimpleEntry<String, Object> entry = i.next();
                    if (entry.getKey() == change.getKey()) {
                        i.remove();
                    }
                }
                AbstractMap.SimpleEntry<String, Object> updated = new AbstractMap.SimpleEntry<String, Object>(change.getKey(),change.getValue());
                state.add(updated);
            }
            // if it does not exist in the current state, add it
            else {
                state.add(new AbstractMap.SimpleEntry<String, Object>(change.getKey(),change.getValue()));
            }
        }
        return state;
    }

    /**
     * Used for building up the graph and holding the running costs of actions.
     */
    private class Node {
        public Node parent;
        public float runningCost;
        public HashSet<AbstractMap.SimpleEntry<String, Object>> state;
        public GoapAction action;

        public Node(Node parent, float runningCost, HashSet<AbstractMap.SimpleEntry<String, Object>> state, GoapAction action) {
            this.parent = parent;
            this.runningCost = runningCost;
            this.state = state;
            this.action = action;
        }
    }

}

