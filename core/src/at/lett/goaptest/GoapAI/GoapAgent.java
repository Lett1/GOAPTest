package at.lett.goaptest.GoapAI;

import at.lett.goaptest.Entities.Entity;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by kilianstrommer on 28.05.16.
 */
public class GoapAgent {

    private GoapFSM stateMachine;

    private GoapFSMState idleState; // finds something to do
    private GoapFSMState moveToState; // moves to a target
    private GoapFSMState performActionState; // performs an action

    private HashSet<GoapAction> availableActions;
    private Queue<GoapAction> currentActions;

    private IGoap dataProvider; // this is the implementing class that provides our world data and listens to feedback on planning

    private GoapPlanner planner;


    public GoapAgent (HashSet<GoapAction> actions, IGoap dataProvider) {
        stateMachine = new GoapFSM ();
        availableActions = new HashSet<GoapAction> ();
        currentActions = new LinkedList<>();
        planner = new GoapPlanner ();
        //findDataProvider ();
        this.dataProvider = dataProvider;
        createIdleState ();
        createMoveToState ();
        createPerformActionState ();
        stateMachine.pushState (idleState);
        //loadActions ();
        this.availableActions = actions;
    }


    public void Update (Entity entity) {

        stateMachine.Update(entity);
    }


    public void addAction(GoapAction a) {
        availableActions.add(a);
    }

    public GoapAction getAction(Class<GoapAction> action) {
        for (GoapAction g : availableActions) {
            if (g.getClass().equals(action) )
                return g;
        }
        return null;
    }

    public void removeAction(GoapAction action) {
        availableActions.remove(action);
    }

    private boolean hasActionPlan() {
        return currentActions.size() > 0;
    }

    private void createIdleState() {
        idleState = (fsm, entity) -> {
            // GOAP planning

            // get the world state and the goal we want to plan for
            HashSet<AbstractMap.SimpleEntry<String, Object>> worldState = dataProvider.getWorldState();
            HashSet<AbstractMap.SimpleEntry<String, Object>> goal = dataProvider.createGoalState();

            // Plan
            Queue<GoapAction> plan = planner.plan(entity, availableActions, worldState, goal);
            if (plan != null) {
                // we have a plan, hooray!
                currentActions = plan;
                dataProvider.planFound(goal, plan);

                fsm.popState(); // move to PerformAction state
                fsm.pushState(performActionState);

            } else {
                // ugh, we couldn't get a plan
                System.out.println("Planning failed D:");
                dataProvider.planFailed(goal);
                fsm.popState (); // move back to IdleAction state
                fsm.pushState (idleState);
            }

        };
    }

    private void createMoveToState() {
        moveToState = (fsm, entity) -> {
            // move the game object

            GoapAction action = currentActions.peek();
            if (action.requiresInRange() && action.target == null) {
                System.out.println("Action requires a target but has none. Planning failed. You did not assign the target in your Action.checkProceduralPrecondition()");
                fsm.popState(); // move
                fsm.popState(); // perform
                fsm.pushState(idleState);
                return;
            }

            // get the agent to move itself
            if ( dataProvider.moveAgent(action) ) {
                fsm.popState();
            }

			/*MovableComponent movable = (MovableComponent) gameObj.GetComponent(typeof(MovableComponent));
			if (movable == null) {
				Debug.Log("<color=red>Fatal error:</color> Trying to move an Agent that doesn't have a MovableComponent. Please give it one.");
				fsm.popState(); // move
				fsm.popState(); // perform
				fsm.pushState(idleState);
				return;
			}
			float step = movable.moveSpeed * Time.deltaTime;
			gameObj.transform.position = Vector3.MoveTowards(gameObj.transform.position, action.target.transform.position, step);
			if (gameObj.transform.position.Equals(action.target.transform.position) ) {
				// we are at the target location, we are done
				action.setInRange(true);
				fsm.popState();
			}*/
        };
    }

    private void createPerformActionState() {

        performActionState = (fsm, gameObj) -> {
            // perform the action

            if (!hasActionPlan()) {
                // no actions to perform
                System.out.println("Done actions");
                fsm.popState();
                fsm.pushState(idleState);
                dataProvider.actionsFinished();
                return;
            }

            GoapAction action = currentActions.peek();
            if ( action.isDone() ) {
                // the action is done. Remove it so we can perform the next one
                currentActions.poll();
            }

            if (hasActionPlan()) {
                // perform the next action
                action = currentActions.peek();
                boolean inRange = action.requiresInRange() ? action.isInRange() : true;

                if ( inRange ) {
                    // we are in range, so perform the action
                    boolean success = action.perform(gameObj);

                    if (!success) {
                        // action failed, we need to plan again
                        fsm.popState();
                        fsm.pushState(idleState);
                        dataProvider.planAborted(action);
                    }
                } else {
                    // we need to move there first
                    // push moveTo state
                    fsm.pushState(moveToState);
                }

            } else {
                // no actions left, move to Plan state
                fsm.popState();
                fsm.pushState(idleState);
                dataProvider.actionsFinished();
            }

        };
    }

    /*private void findDataProvider() {
        foreach (Component comp in gameObject.GetComponents(typeof(Component)) ) {
            if ( typeof(IGoap).IsAssignableFrom(comp.GetType()) ) {
                dataProvider = (IGoap)comp;
                return;
            }
        }
    }*/

    /*private void loadActions ()
    {
        GoapAction[] actions = gameObject.GetComponents<GoapAction>();
        for (GoapAction a : actions) {
            availableActions.add (a);
        }

        //Debug.Log("Found actions: "+prettyPrint(actions));
    }*/

    /*public static String prettyPrint(HashSet<KeyValuePair<string,object>> state) {
        String s = "";
        foreach (KeyValuePair<string,object> kvp in state) {
            s += kvp.Key + ":" + kvp.Value.ToString();
            s += ", ";
        }
        return s;
    }

    public static string prettyPrint(Queue<GoapAction> actions) {
        String s = "";
        foreach (GoapAction a in actions) {
            s += a.GetType().Name;
            s += "-> ";
        }
        s += "GOAL";
        return s;
    }

    public static string prettyPrint(GoapAction[] actions) {
        String s = "";
        foreach (GoapAction a in actions) {
            s += a.GetType().Name;
            s += ", ";
        }
        return s;
    }

    public static string prettyPrint(GoapAction action) {
        String s = ""+action.GetType().Name;
        return s;
    }*/
}
