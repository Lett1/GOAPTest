package at.lett.goaptest.GoapAI;

import at.lett.goaptest.Entities.Entity;

import java.util.Stack;

/**
 * Created by kilianstrommer on 28.05.16.
 */
public class GoapFSM {

    private Stack<GoapFSMState> stateStack = new Stack<>();

    public void Update(Entity entity)
    {
        if(stateStack.peek() != null)
            stateStack.peek().Update(this, entity);
    }

    public void pushState(GoapFSMState state) {
        stateStack.add(state);
    }

    public void popState() {
        stateStack.pop();
    }
}
