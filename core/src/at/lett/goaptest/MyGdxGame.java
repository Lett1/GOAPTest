package at.lett.goaptest;

import at.lett.goaptest.Entities.Cuby;
import at.lett.goaptest.Entities.Entity;
import at.lett.goaptest.Entities.Forest;
import at.lett.goaptest.Pathfinder.BreadthFirstSearch;
import at.lett.goaptest.Pathfinder.GridCell;
import at.lett.goaptest.Pathfinder.GridNode;
import at.lett.goaptest.Pathfinder.SquareGrid;
import at.lett.goaptest.Singletons.EntityManager;
import at.lett.goaptest.Singletons.Pathfinder;
import at.lett.goaptest.util.OrthoCamController;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import at.lett.goaptest.util.PerlinNoiseGenerator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyGdxGame extends ApplicationAdapter {
    private TiledMap map;
    private TiledMapRenderer renderer;
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera camera;
    private OrthoCamController cameraController;
    private AssetManager assetManager;
    private Texture tiles;
    private Texture texture;
    private BitmapFont font;
    private SpriteBatch batch;
    private float[][] heightMap;
    private HashMap<Vector2, GridCell> navmesh;
    private BreadthFirstSearch bs; //TODO: Remove after debugging
    private SquareGrid grid; //TODO: Move somewhere else


    private Texture arrow_texture;
    private TextureRegion[][] arrows;

    @Override
    public void create () {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        int map_size = 128;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, (w / h) * 320, 320);
        camera.zoom = 10f;
        camera.update();

        font = new BitmapFont();
        batch = new SpriteBatch();


        heightMap = PerlinNoiseGenerator.generatePerlinNoise(map_size, map_size, 6);

        for (int i = 0; i < heightMap.length; i++) {
            for (int j = 0; j < heightMap[i].length; j++) {
                float height = heightMap[i][j];
                float distanceX = ((map_size/2 - 1) - i) * ((map_size/2 - 1) - i);
                float distanceY = ((map_size/2 - 1) - j) * ((map_size/2 - 1) - j);
                float distanceToCenter = (float)Math.sqrt(distanceX + distanceY) / (float)map_size;

                heightMap[i][j] = MathUtils.clamp(height - distanceToCenter, 0, 1);
            }
        }

        {
            tiles = new Texture(Gdx.files.internal("tiles/tiles.png"));
            TextureRegion[][] splitTiles = TextureRegion.split(tiles, 16, 16);
            map = new TiledMap();
            MapLayers layers = map.getLayers();
            for (int l = 0; l < 20; l++) {
                TiledMapTileLayer layer = new TiledMapTileLayer(map_size, map_size, 16, 16);
                for (int x = 0; x < layer.getWidth(); x++) {
                    for (int y = 0; y < layer.getHeight(); y++) {
                        Cell cell = new Cell();
                        float height = heightMap[x][y];
                        if(height < 0.2)
                        {
                            cell.setTile(new StaticTiledMapTile(splitTiles[0][5]));
                        }
                        else if(height > 0.2 && height < 0.3)
                        {
                            cell.setTile(new StaticTiledMapTile(splitTiles[0][3]));
                        }
                        else if(height > 0.3 && height < 0.35)
                        {
                            cell.setTile(new StaticTiledMapTile(splitTiles[0][2]));
                        }
                        else if(height > 0.35 && height < 0.6)
                        {
                            cell.setTile(new StaticTiledMapTile(splitTiles[0][1]));
                        }
                        else
                        {
                            cell.setTile(new StaticTiledMapTile(splitTiles[0][4]));
                        }

                        layer.setCell(x, y, cell);
                    }
                }
                layers.add(layer);
            }
        }

        float[][] biomeMap = PerlinNoiseGenerator.generatePerlinNoise(map_size, map_size, 6);
        for (int i = 0; i < biomeMap.length; i++) {
            for (int j = 0; j < biomeMap[i].length; j++) {
                if(heightMap[i][j] > 0.4 && heightMap[i][j] < 0.6)
                {
                    if(biomeMap[i][j] > 0.5)
                        EntityManager.getInstance().entities.add(new Forest(new Vector2(i,j)));
                }
            }
        }

        //EntityManager.getInstance().entities.add(new Cuby());

        Pathfinder.getInstance().init(map_size, map_size, heightMap);
        Pathfinder.getInstance().updateGrid(EntityManager.getInstance().entities);

        renderer = new OrthogonalTiledMapRenderer(map);
        shapeRenderer = new ShapeRenderer();



        //navmesh = bs.getPath(grid, new Vector2(map_size/2-1, map_size/2-1), new Vector2(2,2));

        arrow_texture = new Texture(Gdx.files.internal("arrows.png"));
        arrows = TextureRegion.split(arrow_texture, 16, 16);

        cameraController = new OrthoCamController(camera, grid);
        Gdx.input.setInputProcessor(cameraController);

    }

    @Override
    public void render () {

        update();

        Gdx.gl.glClearColor(100f / 255f, 100f / 255f, 250f / 255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();

        renderer.setView(camera);
        renderer.render();

        if(cameraController.debugSettings.get("showHeightmap")) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setProjectionMatrix(camera.combined);
            for (int i = 0; i < heightMap.length; i++) {
                for (int j = 0; j < heightMap[i].length; j++) {
                    float height = heightMap[i][j];
                    shapeRenderer.setColor(height, height, height, 0);
                    shapeRenderer.rect(i * 16, j * 16, 16, 16);
                }
            }
            shapeRenderer.end();
        }

        if(cameraController.debugSettings.get("showPathing"))
        {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setProjectionMatrix(camera.combined);

            DefaultGraphPath<GridNode> path = Pathfinder.getInstance().getPath(cameraController.getMousePos(),cameraController.lastClick);

            for(GridNode node: path.nodes)
            {
                shapeRenderer.setColor(com.badlogic.gdx.graphics.Color.GREEN);
                shapeRenderer.circle(node.mX*16, node.mY*16, 8);
            }

            shapeRenderer.end();
        }

        batch.begin();

        for(Entity entity : EntityManager.getInstance().entities)
        {
            entity.draw(batch);
        }

        font.draw(batch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, 20);
        batch.setProjectionMatrix(camera.combined);
        batch.end();
    }

    public void update()
    {
        float delta = Gdx.graphics.getDeltaTime();

        for(Entity entity : EntityManager.getInstance().entities)
        {
            entity.update(delta);
        }

        EntityManager.removeDeadEntities();
        EntityManager.processNewEntities();
        //Pathfinder.getInstance().updateGrid(EntityManager.getInstance().entities);
    }
}