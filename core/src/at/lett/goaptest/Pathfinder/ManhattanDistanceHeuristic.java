package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.ai.pfa.Heuristic;

/**
 * Created by kilianstrommer on 01.06.16.
 */
public class ManhattanDistanceHeuristic implements Heuristic<GridNode> {

    @Override
    public float estimate(GridNode node, GridNode endNode) {
        return Math.abs(endNode.mX - node.mX) + Math.abs(endNode.mY - node.mY);
    }
}
