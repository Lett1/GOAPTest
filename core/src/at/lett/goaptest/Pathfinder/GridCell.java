package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by kilianstrommer on 21.05.16.
 */
public class GridCell {
    public Vector2 position;
    public boolean passable;
    public float height;
    public float score;
    public float cost;
    public boolean occupied;

    public GridCell(Vector2 position, boolean passable, float height, float cost)
    {
        this.position = position;
        this.passable = passable;
        this.height = height;
        this.score = 0f;
        this.cost = cost;
        this.occupied = false;
    }

    @Override
    public String toString() {
        return position.toString() + "; " + height;
    }
}
