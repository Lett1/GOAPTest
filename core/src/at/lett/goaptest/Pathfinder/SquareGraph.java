package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.ai.pfa.indexed.DefaultIndexedGraph;

/**
 * Created by kilianstrommer on 01.06.16.
 */
public class SquareGraph extends DefaultIndexedGraph<GridNode> {

    /**
     * @param aSize Just an estimate of size. Will grow later if needed.
     */
    public SquareGraph(int aSize) {
        super(aSize);
    }

    public void addNode(GridNode aNodes) {
        nodes.add(aNodes);
    }

    public GridNode getNode(int aIndex) {
        return nodes.get(aIndex);
    }
}
