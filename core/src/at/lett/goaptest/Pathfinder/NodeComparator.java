package at.lett.goaptest.Pathfinder;

import java.util.Comparator;

/**
 * Created by kilianstrommer on 24.05.16.
 */
public class NodeComparator implements Comparator<GridCell> {

    @Override
    public int compare(GridCell x, GridCell y) {
        if (x.score < y.score)
        {
            return -1;
        }
        if (x.score > y.score)
        {
            return 1;
        }
        return 0;
    }


}
