package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by kilianstrommer on 21.05.16.
 */
public class SquareGrid {
    private int width;
    private int height;
    public GridCell[][] nodes;

    private static final int[][] NEIGHBOURS_4 = {
            {+1, 0}, {0, -1}, {-1, 0}, {0, +1}
    };

    private static final int[][] NEIGHBOURS_8 = {
            {-1, +1}, {0, +1}, {+1, +1},
            {-1, 0},           {+1, 0},
            {-1, -1}, {0, -1}, {+1, -1}
    };

    public SquareGrid(int width, int height, float[][] heightMap)
    {
        this.width = width;
        this.height = height;
        this.nodes = new GridCell[width][height];
        for (int i = 0; i < this.nodes.length; i++) {
            for (int j = 0; j < this.nodes[i].length; j++) {
                this.nodes[i][j] = new GridCell(new Vector2(i, j), heightMap[i][j] < 0.2 ? false : true, heightMap[i][j], assignCost(heightMap[i][j]));
            }
        }
    }

    public boolean isInBounds(float x, float y)
    {
        return (x >= 0 && x < this.width) && (y >= 0 && y < this.height);
    }

    private boolean isPassable(float x, float y)
    {
        return getCell((int)x, (int)y).passable;
    }

    public GridCell getCell(int x, int y)
    {
        return nodes[x][y];
    }

    public void setCell(int x, int y, GridCell cell) { nodes[x][y] = cell;}

    private float assignCost(float height)
    {
        if(height < 0.2)
        {
            return Float.POSITIVE_INFINITY;
        }
        else if(height > 0.2 && height < 0.3)
        {
            return 1f;
        }
        else if(height > 0.3 && height < 0.6)
        {
            return 0f;
        }
        else
        {
            return 10f + height*5;
        }
    }

    public float calculateCost(GridCell from_node, GridCell to_node)
    {
        return from_node.cost - to_node.cost;
    }

    public ArrayList<GridCell> getNeighbours(Vector2 position)
    {
        ArrayList<GridCell> results = new ArrayList<>();
        for (int[] offset : NEIGHBOURS_4) {
            if (isInBounds(position.x + offset[1], position.y + offset[0]) &&
                    isPassable(position.x + offset[1], position.y + offset[0])) {
                results.add(getCell((int)position.x + offset[1],(int)position.y + offset[0]));
            }
        }

        if((position.x + position.y) % 2 == 0)
            Collections.reverse(results);

        return results;
    }

    public ArrayList<GridCell> getNeighbours8(Vector2 position)
    {
        ArrayList<GridCell> results = new ArrayList<>();
        for (int[] offset : NEIGHBOURS_8) {
            if (isInBounds(position.x + offset[1], position.y + offset[0]) &&
                    isPassable(position.x + offset[1], position.y + offset[0])) {
                results.add(getCell((int)position.x + offset[1],(int)position.y + offset[0]));
            }
        }

        if((position.x + position.y) % 2 == 0)
            Collections.reverse(results);

        return results;
    }

}
