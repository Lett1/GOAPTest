package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.ai.pfa.indexed.IndexedNode;
import com.badlogic.gdx.utils.Array;

/**
 * Created by kilianstrommer on 01.06.16.
 */
public class GridNode implements IndexedNode<GridNode> {

    /** Index that needs to be unique for every node and starts from 0. */
    private int mIndex;

    /** Whether or not this tile is in the path. */
    private boolean mSelected = false;

    /** X pos of node. */
    public final int mX;
    /** Y pos of node. */
    public final int mY;

    /** The neighbours of this node. i.e to which node can we travel to from here. */
    Array<Connection<GridNode>> mConnections = new Array<Connection<GridNode>>();

    /** @param aIndex needs to be unique for every node and starts from 0. */
    public GridNode(int aX, int aY, int aIndex) {
        mIndex = aIndex;
        mX = aX;
        mY = aY;
    }

    @Override
    public int getIndex() {
        return mIndex;
    }

    @Override
    public Array<Connection<GridNode>> getConnections() {
        return mConnections;
    }

    public void addNeighbour(GridNode aNode) {
        if (null != aNode) {
            mConnections.add(new DefaultConnection<GridNode>(this, aNode));
        }
    }

    public String toString() {
        return String.format("Index:%d x:%d y:%d", mIndex, mX, mY);
    }
}
