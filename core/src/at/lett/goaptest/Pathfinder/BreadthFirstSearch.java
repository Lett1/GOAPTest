package at.lett.goaptest.Pathfinder;

import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Created by kilianstrommer on 22.05.16.
 */
public class BreadthFirstSearch {

    private PriorityQueue<GridCell> frontier;
    public HashMap<Vector2, GridCell> came_from;
    private HashMap<Vector2, Float> cost_so_far;

    public BreadthFirstSearch()
    {
        this.frontier = new PriorityQueue<>(1, new NodeComparator());
        this.came_from = new HashMap<>();
        this.cost_so_far = new HashMap<>();
    }

    private float heuristic(Vector2 a, Vector2 b)
    {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
    }

    public ArrayList<Vector2> getPath(SquareGrid graph, Vector2 start, Vector2 goal)
    {

        start = new Vector2((int)start.x, (int)start.y);

        if(!graph.isInBounds(start.x, start.y))
            return new ArrayList<>();



        frontier.clear();
        frontier.add(graph.getCell((int)start.x, (int)start.y));

        came_from.clear();
        came_from.put(start, null);

        cost_so_far.clear();
        cost_so_far.put(new Vector2((int)start.x, (int)start.y), 0f);
        GridCell current;

        while(!frontier.isEmpty())
        {
            current = frontier.poll();

            if(current.position.epsilonEquals(goal, 0))
                break;

            for(GridCell next : graph.getNeighbours(current.position))
            {
                float new_cost = cost_so_far.get(current.position) + graph.calculateCost(current, next);

                if(!came_from.containsKey(next.position) || new_cost < cost_so_far.get(next.position)) {
                    cost_so_far.put(next.position, new_cost);
                    came_from.put(next.position, current);

                    if(frontier.contains(next))
                    {
                        frontier.remove(next);
                        next.score = new_cost + heuristic(goal, next.position);
                        frontier.add(next);
                    }
                    else
                    {
                        next.score = new_cost + heuristic(goal, next.position);
                        frontier.add(next);
                    }



                }
            }
        }

        if(!came_from.containsKey(goal))
            return new ArrayList<>();
            //return Collections.<Vector2>emptyList();

        Vector2 path_current = goal;
        ArrayList<Vector2> path = new ArrayList<>();
        path.add(path_current);

        while(!path_current.epsilonEquals(start,0))
        {
            path_current = came_from.get(path_current).position;
            path.add(path_current);
        }

        return path;
    }
}
