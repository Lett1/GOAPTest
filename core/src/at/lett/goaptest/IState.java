package at.lett.goaptest;

/**
 * Created by kilianstrommer on 16.02.16.
 */
public interface IState {
    void update(float dt);
    void handleInput();

    void enter(Object[] args);
    void exit();
}
