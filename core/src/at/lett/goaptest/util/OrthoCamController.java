package at.lett.goaptest.util;

/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

        import at.lett.goaptest.Pathfinder.SquareGrid;
        import com.badlogic.gdx.InputAdapter;
        import com.badlogic.gdx.graphics.OrthographicCamera;
        import com.badlogic.gdx.math.MathUtils;
        import com.badlogic.gdx.math.Vector2;
        import com.badlogic.gdx.math.Vector3;

        import java.util.HashMap;

public class OrthoCamController extends InputAdapter {
    final OrthographicCamera camera;
    final SquareGrid grid;
    final Vector3 curr = new Vector3();
    final Vector3 last = new Vector3(-1, -1, -1);
    final Vector3 delta = new Vector3();
    private int mouseX = 0;
    private int mouseY = 0;
    public Vector2 lastClick = new Vector2();
    public HashMap<String, Boolean> debugSettings;

    public OrthoCamController (OrthographicCamera camera, SquareGrid grid) {
        this.camera = camera;
        this.grid = grid;
        //TODO: Move boolean variable out of class
        this.debugSettings = new HashMap<>();
        this.debugSettings.put("showHeightmap", false);
        this.debugSettings.put("showPathing", false);
    }

    public Vector2 getMousePos()
    {
        return new Vector2(mouseX, mouseY);
    }

    private void toggleSetting(String setting)
    {
        debugSettings.put(setting, !debugSettings.get(setting));
    }

    @Override
    public boolean touchDragged (int x, int y, int pointer) {
        camera.unproject(curr.set(x, y, 0));
        if (!(last.x == -1 && last.y == -1 && last.z == -1)) {
            camera.unproject(delta.set(last.x, last.y, 0));
            delta.sub(curr);
            camera.position.add(delta.x, delta.y, 0);
        }
        last.set(x, y, 0);
        return false;
    }

    @Override
    public boolean touchUp (int x, int y, int pointer, int button) {
        last.set(-1, -1, -1);
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 mousepos = camera.unproject(new Vector3(screenX, screenY, 0));

        lastClick = new Vector2((int)mousepos.x/16, (int)mousepos.y/16);
        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        if(amount == -1)
        {
            //scroll up -> zoom in
            camera.zoom -= 0.25f;
        }
        else if(amount == 1)
        {
            //scroll down -> zoom out
            camera.zoom += 0.25;
        }

        camera.zoom = MathUtils.clamp(camera.zoom, 0.1f, 10);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        switch (character)
        {
            case 'f':
                toggleSetting("showHeightmap");
                break;
            case 'p':
                toggleSetting("showPathing");
                break;
            case 'n':
                System.out.println(grid.getNeighbours(new Vector2(mouseX, mouseY)).toString());
                break;
        }

        return false;
    }

    public boolean mouseMoved (int x, int y) {

        Vector3 mousepos = camera.unproject(new Vector3(x, y, 0));
        mouseX = (int)mousepos.x/16;
        mouseY = (int)mousepos.y/16;

        return false;
    }



}