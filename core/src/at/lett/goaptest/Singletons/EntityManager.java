package at.lett.goaptest.Singletons;

import at.lett.goaptest.Entities.Entity;

import java.util.ArrayList;

/**
 * Created by kilianstrommer on 29.05.16.
 */
public class EntityManager {

    public static final EntityManager instance = new EntityManager();
    public static ArrayList<Entity> entities = new ArrayList<>();
    private static ArrayList<Entity> waitlist = new ArrayList<>();

    private EntityManager() {}

    public static EntityManager getInstance()
    {
        return instance;
    }

    public static void addEntity(Entity e)
    {
        waitlist.add(e);
    }

    public static void processNewEntities()
    {
        if(!waitlist.isEmpty())
        {
            entities.addAll(waitlist);
            waitlist.clear();
        }
        Pathfinder.getInstance().updateGrid(entities);
    }

    public static void removeDeadEntities()
    {
        entities.removeIf(p -> p.isDead());
    }


    //TODO: Make that into a function

    /*public <T extends Entity> List<T> getEntitiesByType(Class type)
    {
        return entities.stream()
                .filter(x -> type.isInstance(x))
                .map(x -> type.cast(x))
                .collect(Collectors.toList());
    }*/


}
