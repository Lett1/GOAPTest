package at.lett.goaptest.Singletons;

import at.lett.goaptest.Entities.Entity;
import at.lett.goaptest.Pathfinder.*;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by kilianstrommer on 29.05.16.
 */
public class Pathfinder {

    public static final Pathfinder instance = new Pathfinder();
    public static SquareGraph graph;

    private static IndexedAStarPathFinder<GridNode> pathfinder;
    private static ManhattanDistanceHeuristic heuristic;

    private static GridNode[][] nodes;

    private Pathfinder() {}

    public static Pathfinder getInstance()
    {
        return instance;
    }

    public static void init(int width, int height, float[][] heightMap)
    {
        graph = new SquareGraph((width * height) /2);
        nodes = new GridNode[width][height];

        int index = 0;

        for (int i = 0; i < heightMap.length; i++) {
            for (int j = 0; j < heightMap[i].length; j++) {
                float fHeight = heightMap[i][j];

                if(fHeight > 0.2)
                {
                    nodes[i][j] = new GridNode(i,j,index);
                    graph.addNode(nodes[i][j]);
                    index++;
                }
            }
        }

        for (int x = 0; x < nodes.length; x++) {
            for (int y = 0; y < nodes[0].length; y++) {
                if (null != nodes[x][y]) {
                    addNodeNeighbour(nodes[x][y], x - 1, y); // Node to left
                    addNodeNeighbour(nodes[x][y], x + 1, y); // Node to right
                    addNodeNeighbour(nodes[x][y], x, y - 1); // Node below
                    addNodeNeighbour(nodes[x][y], x, y + 1); // Node above
                }
            }
        }

        pathfinder = new IndexedAStarPathFinder<GridNode>(graph, true);
        heuristic = new ManhattanDistanceHeuristic();
    }

    /**
     * Add connections to the node at aX aY. If there is no node present at those
     * coordinates no connection will be created.
     * @param aNode The node to connect from.
     * @param aX x coordinate of connecting node.
     * @param aY y coordinate of connecting node.
     */
    private static void addNodeNeighbour(GridNode aNode, int aX, int aY) {
        // Make sure that we are within our array bounds.
        if (aX >= 0 && aX < nodes.length && aY >=0 && aY < nodes[0].length) {
            aNode.addNeighbour(nodes[aX][aY]);
        }
    }

    private static GridNode getNode(Vector2 position)
    {
        return nodes[(int)position.x][(int)position.y];
    }

    private static GridNode getNode(int x, int y)
    {
        return nodes[x][y];
    }

    public static DefaultGraphPath<GridNode> getPath(Vector2 start, Vector2 goal)
    {
        DefaultGraphPath<GridNode> path = new DefaultGraphPath<>();

        GridNode startNode = getNode(start);
        GridNode goalNode  = getNode(goal);

        if(startNode == null || goalNode == null)
            return path;

        pathfinder.searchNodePath(startNode, goalNode, heuristic, path);

        return path;
    }

    public void updateGrid(ArrayList<Entity> entities)
    {
        for(Entity ent: entities)
        {
            /*GridCell cell = grid.getCell((int)ent.getPosition().x,(int)ent.getPosition().y);
            cell.occupied = ent.doesOccupySpace();

            if(ent.doesModifyPathing())
            {
                cell.cost += ent.getPathingCost();
            }

            grid.setCell((int)ent.getPosition().x,(int)ent.getPosition().y, cell);*/
        }
    }

    public Vector2 getFreeSpace(Vector2 position)
    {
        /*Queue<GridCell> frontier = new LinkedList<>();
        ArrayList<Vector2> visited = new ArrayList<>();

        frontier.add(grid.getCell((int)position.x,(int)position.y));

        while(!frontier.isEmpty())
        {
            GridCell current = frontier.poll();

            ArrayList<GridCell> neighbours = grid.getNeighbours8(current.position);
            long openspaces = neighbours.stream().filter(p -> p.occupied || p.passable).count();

            if(openspaces > 0)
                return neighbours.get(MathUtils.random(neighbours.size()-1)).position;
            else

            for(GridCell next : neighbours)
            {
                if(!visited.contains(next.position))
                {
                    frontier.add(next);
                    visited.add(next.position);
                }
            }

            System.out.println(neighbours.size());
        }*/

        return null;

    }


}
