package at.lett.goaptest.GoapActions;

import at.lett.goaptest.Entities.Cuby;
import at.lett.goaptest.Entities.Entity;
import at.lett.goaptest.Entities.Forest;
import at.lett.goaptest.Singletons.EntityManager;
import at.lett.goaptest.GoapAI.GoapAction;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kilianstrommer on 28.05.16.
 */
public class CutWoodAction extends GoapAction {

    private boolean hasLogs = false;
    private Forest targetForest;

    public CutWoodAction()
    {
        addPrecondition("hasLogs", false);
        addEffect("hasLogs", true);
    }

    @Override
    public boolean perform(Entity agent) {

        Cuby cuby = (Cuby)agent;

        if(targetForest.treeCount > 0)
        {
            targetForest.treeCount--;
            hasLogs = true;
            cuby.inventory.put("Logs", 1);

            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean isDone() {
        return hasLogs;
    }

    @Override
    public void reset() {
        hasLogs = false;
        targetForest = null;
    }

    @Override
    public boolean requiresInRange() {
        return true;
    }

    @Override
    public boolean checkProceduralPrecondition(Entity agent) {

        List<Forest> forests= EntityManager.getInstance().entities.stream()
                .filter(x -> x instanceof Forest)
                .map(x -> (Forest)x)
                .collect(Collectors.toList());

        Forest closest = null;
        float closestDist = 0;

        for(Forest forest : forests)
        {
            if(forest.treeCount > 0)
            {
                if(closest == null)
                {
                    closest = forest;
                    //sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
                    float distanceX = (forest.position.x - agent.getPosition().x) * (forest.position.x - agent.getPosition().x);
                    float distanceY = (forest.position.y - agent.getPosition().y) * (forest.position.y - agent.getPosition().y);
                    closestDist = (float)Math.sqrt(distanceX + distanceY);
                }
                else
                {
                    float distanceX = (forest.position.x - agent.getPosition().x) * (forest.position.x - agent.getPosition().x);
                    float distanceY = (forest.position.y - agent.getPosition().y) * (forest.position.y - agent.getPosition().y);
                    float dist = (float)Math.sqrt(distanceX + distanceY);

                    if(dist < closestDist)
                    {
                        closest = forest;
                        closestDist = dist;
                    }
                }
            }
        }

        System.out.println("Closest forest at " + closest.position.toString());

        if(closest == null)
            return false;

        targetForest = closest;
        target = closest;

        return closest != null;
    }


}
