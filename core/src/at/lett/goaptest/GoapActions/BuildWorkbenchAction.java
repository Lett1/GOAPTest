package at.lett.goaptest.GoapActions;

import at.lett.goaptest.Entities.*;
import at.lett.goaptest.Singletons.EntityManager;
import at.lett.goaptest.GoapAI.GoapAction;
import at.lett.goaptest.Singletons.Pathfinder;
import com.badlogic.gdx.math.Vector2;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by kilianstrommer on 28.05.16.
 */
public class BuildWorkbenchAction extends GoapAction {

    private boolean hasWorkbench = false;

    public BuildWorkbenchAction()
    {
        addPrecondition("hasLogs", true);
        addEffect("hasWorkbench", true);
    }

    @Override
    public boolean perform(Entity agent) {

        Cuby cuby = (Cuby)agent;

        /*if(targetForest.treeCount > 0)
        {
            targetForest.treeCount--;
            hasLogs = true;
            cuby.inventory.put("Logs", 1);

            return true;
        }
        else
        {
            return false;
        }*/

        cuby.inventory.put("Logs", 0);

        EntityManager.getInstance().addEntity(new Workbench(target.getPosition()));
        hasWorkbench = true;

        return true;
    }

    @Override
    public boolean isDone() {
        return hasWorkbench;
    }

    @Override
    public void reset() {
        hasWorkbench = false;
    }

    @Override
    public boolean requiresInRange() {
        return true;
    }

    @Override
    public boolean checkProceduralPrecondition(Entity agent) {

        Vector2 space = Pathfinder.getInstance().getFreeSpace(agent.getPosition());

        System.out.println("Free space at " + space.toString());

        if(space == null)
            return false;

        //targetForest = closest;
        target = new Target(space);

        return space != null;
    }


}
