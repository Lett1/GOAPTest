package at.lett.goaptest;

import java.util.Hashtable;

/**
 * Created by kilianstrommer on 16.02.16.
 */
class EmptyState implements IState {
    @Override
    public void update(float dt) {

    }

    @Override
    public void handleInput() {

    }

    @Override
    public void enter(Object[] args) {

    }

    @Override
    public void exit() {

    }
}

public class StateMachine
{
    Hashtable<String, IState> _stateDict = new Hashtable<String, IState>();
    IState _current = new EmptyState();
    public String _currentID = null;

    public IState current() { return _current; }
    public void add(String id, IState state) { _stateDict.put(id, state); }
    public void remove(String id) { _stateDict.remove(id); }
    public void clear() { _stateDict.clear(); }


    public void change(String id, Object[] args)
    {
        _current.exit();
        IState next = _stateDict.get(id);
        next.enter(args);
        _currentID = id;
        _current = next;
    }

    public void update(float dt)
    {
        _current.update(dt);
    }

    public void handleInput()
    {
        _current.handleInput();
    }
}
