package at.lett.goaptest.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kilianstrommer on 26.05.16.
 */
public class Forest implements Entity {

    public int treeCount;
    private Texture texture;
    public Vector2 position;
    public boolean alive = true;

    public Forest(Vector2 position)
    {
        this.treeCount = 10;
        this.position = position;
        this.texture = new Texture(Gdx.files.internal("forest.png"));
    }

    @Override
    public void update(float delta) {

        if(treeCount <= 0)
        {
            this.alive = false;
        }

    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(texture, position.x * texture.getWidth(), position.y * texture.getHeight());

    }

    @Override
    public boolean interact(Entity activator) {
        return true;
    }

    @Override
    public Vector2 getPosition() {
        return this.position;
    }

    @Override
    public boolean doesModifyPathing() {
        return true;
    }

    @Override
    public float getPathingCost() {
        return 5f;
    }

    @Override
    public boolean doesOccupySpace() {
        return true;
    }

    @Override
    public boolean isDead() {
        return !alive;
    }
}
