package at.lett.goaptest.Entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kilianstrommer on 26.05.16.
 */
public interface Entity {

    void update(float delta);
    void draw(SpriteBatch batch);
    boolean interact(Entity activator);
    default boolean isDead() { return false;}

    boolean doesModifyPathing();
    default float getPathingCost() { return 0f;}
    boolean doesOccupySpace();

    Vector2 getPosition();
}
