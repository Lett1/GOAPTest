package at.lett.goaptest.Entities;

import at.lett.goaptest.GoapActions.BuildWorkbenchAction;
import at.lett.goaptest.GoapActions.CutWoodAction;
import at.lett.goaptest.GoapAI.GoapAction;
import at.lett.goaptest.GoapAI.GoapAgent;
import at.lett.goaptest.GoapAI.IGoap;
import at.lett.goaptest.Singletons.Pathfinder;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Path;
import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Created by kilianstrommer on 19.05.16.
 */
public class Cuby implements Entity, IGoap {

    private GoapAgent agent;

    private Vector2 position;

    public HashMap<String, Integer> inventory;
    private HashSet<GoapAction> actions;
    private Texture texture;

    private ArrayList<Vector2> path;
    private Vector2 nextpath;

    public boolean alive = true;

    public Cuby()
    {
        this.texture = new Texture(Gdx.files.internal("cuby.png"));

        this.position = new Vector2(64,64);
        inventory = new HashMap<>();

        actions = new HashSet<>();
        actions.add(new CutWoodAction());
        actions.add(new BuildWorkbenchAction());
        agent = new GoapAgent(actions, this);


    }

    @Override
    public void update(float delta) {
        agent.Update(this);
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(texture, this.position.x * texture.getWidth(), this.position.y * texture.getHeight());
    }

    @Override
    public boolean interact(Entity activator) {
        return true;
    }

    @Override
    public Vector2 getPosition() {
        return this.position;
    }

    @Override
    public boolean doesOccupySpace() {
        return false;
    }

    @Override
    public boolean doesModifyPathing() {
        return false;
    }

    @Override
    public boolean isDead() {
        return !alive;
    }

    @Override
    public HashSet<AbstractMap.SimpleEntry<String, Object>> getWorldState() {
        HashSet<AbstractMap.SimpleEntry<String, Object>> worldData = new  HashSet<AbstractMap.SimpleEntry<String, Object>> ();

        worldData.add(new AbstractMap.SimpleEntry<String, Object>("hasLogs", (inventory.getOrDefault("Logs", 0) > 0)));

        return worldData;
    }

    @Override
    public HashSet<AbstractMap.SimpleEntry<String, Object>> createGoalState() {
        HashSet<AbstractMap.SimpleEntry<String, Object>> goal = new  HashSet<AbstractMap.SimpleEntry<String, Object>> ();

        //goal.add(new AbstractMap.SimpleEntry<String, Object>("hasLogs", true));
        goal.add(new AbstractMap.SimpleEntry<String, Object>("hasWorkbench", true));

        return goal;
    }

    @Override
    public void planFailed(HashSet<AbstractMap.SimpleEntry<String, Object>> failedGoal) {

    }

    @Override
    public void planFound(HashSet<AbstractMap.SimpleEntry<String, Object>> goal, Queue<GoapAction> actions) {
        System.out.println("Found plan!");
    }

    @Override
    public void actionsFinished() {

    }

    @Override
    public void planAborted(GoapAction aborter) {
        System.out.println("Plan aborted! D:");
    }

    @Override
    public boolean moveAgent(GoapAction nextAction) {

        System.out.println("Moving to " + nextAction.target.getPosition().toString());

        float speed = 0.5f;



        if(path == null)
            //path = Pathfinder.getInstance().getPath(this.position, nextAction.target.getPosition());

        if(path.isEmpty()) {
            position.add(1, 0);
            return false;
        }



        if(nextpath == null)
            nextpath = path.remove(0);

        Vector2 direction = new Vector2();
        direction.set(nextpath).sub(this.position).nor().scl(speed);
        this.position.add(direction);

        if(position.epsilonEquals(nextpath,1))
            nextpath = null;


        if(this.position.dst(nextAction.target.getPosition()) <= 1) //TODO: change to 0
        {
            path = null;
            nextAction.setInRange(true);
            return true;
        }
        else
        {
            return false;
        }
    }
}
