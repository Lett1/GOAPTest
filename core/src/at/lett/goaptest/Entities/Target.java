package at.lett.goaptest.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by kilianstrommer on 26.05.16.
 */
public class Target implements Entity {

    public Vector2 position;

    public Target(Vector2 position)
    {
        this.position = position;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void draw(SpriteBatch batch) {
    }

    @Override
    public boolean interact(Entity activator) {
        return true;
    }

    @Override
    public Vector2 getPosition() {
        return this.position;
    }

    @Override
    public boolean doesModifyPathing() {
        return false;
    }

    @Override
    public float getPathingCost() {
        return 0f;
    }

    @Override
    public boolean doesOccupySpace() {
        return false;
    }
}
